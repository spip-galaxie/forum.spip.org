<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissementforum' => '<b>Rogad byin</b> Bann forom sitweb-la l&eacute; byin gayar. Ni di mersi zot tout po sak i f&eacute; roul ron tout bann zespas lantr&egrave;d-la.<p>Somansa, kank tout bann forom-la i grandi, l&eacute; pli difisil po swiv &eacute; lir tout bann kontribisyon. Akoz sa, &eacute; pou rann tout zot forompli gadyamb, ni domann azot prangard zot i swi byin ind&eacute; rokomandasyon ni v&eacute; f&eacute; azot&nbsp;:<br><img src=\'puce.gif\' border=\'0\'> avan zot i m&egrave;t ansanm inn nouvo sij&eacute; pou koz&eacute; si bann forom, mersi zot gard si bann dalon la pa d&eacute;za koz&eacute; isi-minm si bann soz-la ou v&eacute; abord&eacute;&nbsp;;<br><img src=\'puce.gif\' border=\'0\'> v&eacute;y byin zot i poz byin son k&eacute;styon dann la ribrik pr&eacute;siz pou lo sij&eacute; ou v&eacute; abord. ',
'avertissementtitre' => 'V&eacute;y byin ou la donn inn titr pr&eacute;si &eacute; kl&egrave;r pou zot k&eacute;styon.Akoz i va rann pli fasil la navigasyon tout bann zot vizit&egrave;r dann bann forom.',


// D
'download' => 'Apiy t&egrave;rla po t&eacute;l&eacute;sarz nout d&egrave;rny&egrave;r v&egrave;rsyon',


// I
'interetquestion' => 'Di anou koman ou pans k&eacute;styon la l&eacute; itil pou zot',
'interetreponse' => 'Di anou koman ou pans r&eacute;pons-la l&eacute; itil pou zot',


// N
'notermessages' => 'Pou rann forom-la pli vivan, ou p&eacute; indik out lint&eacute;r&eacute; po sakin k&eacute;styon &eacute; r&eacute;pons n&eacute;na andan-la. Li sra itil pou bann zot vizit&egrave;r. <p>Pr&eacute;siz sirtou koman out lint&eacute;r&eacute; si lo paz-la an z&eacute;n&eacute;ral(anon vwar anl&egrave;r paz-la). Ou p&eacute; don in &laquo;&nbsp;note&nbsp;&raquo; po sak r&eacute;pons&nbsp;; i prm&eacute; f&eacute; sortir bann r&eacute;pons i r&eacute;pon my&eacute; lo probl&egrave;m.',
'nouvellequestion' => 'Poz inn nouv&egrave;l k&eacute;styon',
'nouvellereponse' => 'R&eacute;ponn k&eacute;styon-la',


// Q
'questions' => 'Bann k&eacute;styon',
'quoideneuf' => 'Kosa la sanz bann d&eacute;rny&eacute; tan',


// R
'rechercher' => 'Pou rod',
'rechercher_forums' => 'Rod andan bann forom',
'rechercher_tout_site' => 'lo sit anty&eacute;',
'reponses' => 'Bann r&eacute;pons',


// S
'squelette' => 'Apiy po t&eacute;l&eacute;sarz l&eacute;sk&eacute;let lapaz-la',


// T
'thememessage' => 'Lo t&egrave;m forom-la i abord :',
'traductions' => 'Bann tradiksyon pou lo t&eacute;ks-la :'

);


?>
