<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissement_code_forum' => 'Para insertar c&oacute;digo o valorizar tus soluciones, puedes utilizar los atajos tipogr&aacute;ficos  siguientes:<ul><li>&lt;code&gt;... una o varias l&iacute;neas de c&oacute;digo...&lt;/code&gt;</li><li>&lt;cadre&gt;... c&oacute;digo que tiene l&iacute;neas muy largas ...&lt;/cadre&gt;</li></ul>',
'avertissementforum' => '<b>OjO</b> Los foros de este sitio son muy activos. Ante todo, se agradece a quienes animan y enriquecen estos espacios de ayuda mutua. <p>No obstante, cuanto m&aacute;s activos son los foros, m&aacute;s son dificiles de seguir y de consultar. Para que se vuelvan m&aacute;s agradables te agradecemos sigas las siguientes recomendaciones:   <br><img src=\'puce.gif\' border=\'0\'> antes de lanzar un nuevo tema de discusi&oacute;n, verifica que no fu&eacute; abordado anteriormente;<br><img src=\'puce.gif\' border=\'0\'>vigila que haces tu pregunta en la secci&oacute;n que corresponde.',
'avertissementtitre' => 'Cuida en darle un t&iacute;tulo expl&iacute;cito a tu pregunta, para luego facilitar la navegaci&oacute;n de los visitantes de los foros. ',


// B
'barre_cadre' => 'Eucuadrar un texto',
'barre_code' => 'Insertar c&oacute;digo',


// D
'download' => 'Descargar la &uacute;ltima versi&oacute;n',


// I
'info_tag_forum' => 'Puedes etiquetar esta p&aacute;gina de foro con palabras claves que te parecen importantes; le permitir&aacute;n a las pr&oacute;ximos visitantes ubicarse mejor.',
'interetquestion' => 'Indica el inter&eacute;s que tiene para ti esta pregunta ',
'interetreponse' => 'Indica el inter&eacute;s que tiene para ti esta respuesta ',
'inutile' => 'in&uacute;til',


// M
'merci' => 'gracias',


// N
'nouvellequestion' => 'Hacer una nueva pregunta',
'nouvellereponse' => 'Responder a la pregunta',


// P
'page_utile' => 'Esta p&aacute;gina fue para t&iacute;:',


// Q
'questions' => 'Preguntas',
'quoideneuf' => 'Modificaciones recientes',


// R
'rechercher' => 'Buscar',
'rechercher_forums' => 'Buscar en los foros',
'rechercher_tout_site' => 'el sitio entero',
'reponses' => 'Respuestas',


// T
'thememessage' => 'Tema de este foro:',
'traductions' => 'Traducci&oacute;n de este texto:',


// U
'utile' => '&uacute;til'

);


?>
