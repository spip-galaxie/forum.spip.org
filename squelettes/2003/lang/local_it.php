<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissement_code_forum' => 'Per inserire parte di codice o evidenziare le proprie soluzioni &egrave; possibile utilizzare le seguenti scorciatoie tipografiche:<ul><li>&lt;code&gt;... una o pi&ugrave; linee di codice ...&lt;/code&gt;</li><li>&lt;cadre&gt;... codice con linee molto lunghe ...&lt;/cadre&gt;</li></ul>',
'avertissementforum' => '<b>N.B.</b> I forum di questo sito sono molto attivi. Ringraziamo tutti coloro che animano e arricchiscono questi spazi dedicati al confronto e allo scambio reciproco.<p>Tuttavia, pi&ugrave; un forum &egrave; attivo e pi&ugrave; diventa difficile la sua consultazione. Al fine di rendere questi forum pi&ugrave; fruibili vi preghiamo di seguire queste raccomandazioni&nbsp;:<br><img src=\'puce.gif\' border=\'0\'> prima di iniziare un nuovo argomento di discussione, verificare che tale argomento non sia stato gi&agrave; trattato&nbsp;;<br><img src=\'puce.gif\' border=\'0\'> assicurarsi di porre la domanda nella rubrica appropriata.',
'avertissementtitre' => '<b>Assicurarsi di dare <font color=\'red\'>un titolo esplicito alla propria domanda</font> per facilitare la navigazione successiva agli altri visitatori dei forum.</b><p><font color=\'red\'>I messaggi che non hanno un titolo esplicito verrranno cancellati.</font>',


// B
'barre_cadre' => 'Incorniciare un testo',
'barre_code' => 'Inserire del codice',


// D
'download' => 'Download dell\'ultima versione',


// I
'info_tag_forum' => '&Egrave; possibile etichettare questa pagina di forum con parole chiave che si ritengono importanti. Esse permetteranno ai prossimi visitatori di orientarsi meglio nel sito.',
'interetquestion' => 'Indicare il livello di interesse che si attribuisce a questa domanda',
'interetreponse' => 'Indicare il livello di interesse che si attribuisce a questa risposta',
'inutile' => 'inutile',


// M
'merci' => 'grazie',


// N
'nouvellequestion' => 'Fare una nuova domanda',
'nouvellereponse' => 'Rispondere alla domanda',


// P
'page_utile' => 'Questa pagina vi &egrave; stata:',


// Q
'questions' => 'Domande',
'quoideneuf' => 'Ultime modifiche',


// R
'rechercher' => 'Cerca',
'rechercher_forums' => 'Cercare nei forum',
'rechercher_tout_site' => 'tutto il sito',
'reponses' => 'Risposte',


// T
'thememessage' => 'Tema di questo forum:',
'traductions' => 'Traduzioni di questo testo:',


// U
'utile' => 'utile'

);


?>
