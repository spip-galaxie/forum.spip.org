<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissement_code_forum' => 'Per insertar codi o destacar les vostres solucions, podeu utilitzar les seg&uuml;ents dreceres tipogr&agrave;fiques:<ul><li>&lt;code&gt;... una o diverses l&iacute;nies de codi ...&lt;/code&gt;</li><li>&lt;cadre&gt;... codi que tingui l&iacute;nies molt llargues ...&lt;/cadre&gt;</li></ul>',
'avertissementforum' => '<b>N.B.</b> Els f&ograve;rums d\'aquest lloc son molt actius. Que tots aquells que animen i enriqueixen aquests espais d\'ajuda m&uacute;tua rebin aqu&iacute; el nostre agra&iuml;ment.<p>Aix&ograve; no obstant, com m&eacute;s actius s&oacute;n els f&ograve;rums, m&eacute;s dif&iacute;cils esdevenen de seguir i consultar. Per a fer que aquests f&ograve;rums siguin m&eacute;s agradables, us agrair&iacute;em de seguir aquestes recomanacions:<br><img src=\'puce.gif\' border=\'0\'> abans d\'exposar un nou tema de discussi&oacute;, verifiqueu si el tema no ha estat ja abordat aqu&iacute;;<br><img src=\'puce.gif\' border=\'0\'>tingueu cura de fer la vostra pregunta dins la secci&oacute; que li correspongui.',
'avertissementtitre' => '<b>Tinga cura de donar <font color=\'red\'>un t&iacute;tol expl&iacute;cit per a la questi&oacute;</font> per a facilitar la navegaci&oacute; d\'altres visitants dins dels forums. </b><p><font color=\'red\'>Els missatges amb t&iacute;tols no expl&iacute;cits son esborrats.</font>',


// B
'barre_cadre' => 'Emmarcar el text',
'barre_code' => 'Insertar codi',


// D
'download' => 'Descarregar la darrera versi&oacute;',


// I
'info_tag_forum' => 'Podeu etiquetar aquesta p&agrave;gina del f&ograve;rum amb les paraules clau que us semblin importants. Seran &uacute;tils perqu&egrave; els futurs visitants trobin les respostes m&eacute;s de pressa.',
'interetquestion' => 'Indiqueu l\'inter&eacute;s que us porta a aquesta q&uuml;esti&oacute;',
'interetreponse' => 'Indiqueu l\'inter&eacute;s que us porta a aquesta resposta',
'inutile' => 'in&uacute;til',


// M
'merci' => 'gr&agrave;cies',


// N
'nouvellequestion' => 'Proposar una nova q&uuml;esti&oacute;',
'nouvellereponse' => 'Respondre a la q&uuml;esti&oacute;',


// P
'page_utile' => 'Aquesta p&agrave;gina us ha semblat:',


// Q
'questions' => 'Preguntes',
'quoideneuf' => 'Modificacions recents',


// R
'rechercher' => 'Cercar',
'rechercher_forums' => 'Buscar dins dels f&ograve;rums',
'rechercher_tout_site' => 'tot el web',
'reponses' => 'Respostes',


// T
'thememessage' => ' Tema d\'aquest f&ograve;rum :',
'traductions' => ' Traduccions d\'aquest text :',


// U
'utile' => '&uacute;til'

);


?>
