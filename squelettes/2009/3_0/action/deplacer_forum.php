<?php
// deplacer un thread d'un article a un autre
// thx chryjs et yohann prigent (spipBB)
function action_deplacer_forum() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();
	$r = rawurldecode(_request('redirect'));

	list($id_forum, $agir) = preg_split('/\W/', $arg);
	$id_forum = intval($id_forum);
	$result = sql_select("*", "spip_forum", "id_forum=$id_forum");
	if (!($row = sql_fetch($result)))
		return;

	switch ($agir) {
	case "deplacer" :
		$confirme = _request('confirme');
		$annule = _request('annule');

		if (!$confirme) redirige_par_entete($r);

		// on identifie le forum(article) dans lequel on le deplace et on verifie son existence
		$id_nouvart = _request('nouveau_forum');
		$result = sql_select("*", "spip_articles", "id_article=$id_nouvart");
		if (!($rowart = sql_fetch($result)))
			return;

		@sql_updateq("spip_forum", array("objet" => "article", "id_objet" => $id_nouvart), "id_thread=$id_forum");

		break;
	}
	include_spip('inc/invalideur');
    suivre_invalideur("id='id_forum/a$id_objet'");
}
?>