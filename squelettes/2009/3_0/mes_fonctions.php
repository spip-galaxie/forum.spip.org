<?php

// pour afficher proprement le nom des langues
function afficher_nom_langue($lang) {
	if (preg_match("/^oc(_|$)/", $lang)) {
		return "occitan";
	} else {
		return traduire_nom_langue($lang);
	}
}

function police_des_bavards($score) {
	return round((1 + (log10($score))) / 1.9, 2);
}

// moderer les messages depuis l'espace public
// ne supprime pas le message ni le fil mais les passe en 'off'
// on peut toujours les revalider dans l'espace prive
function invalider_forum($id_forum, $r = '') {
	include_spip("inc/securiser_action");
	list($id_auteur, $pass) = caracteriser_auteur();
	$arg = "$id_forum";
	$hash = _action_auteur("instituer_forum-$arg-off", $id_auteur, $pass, 'alea_ephemere');
	$r = rawurlencode(_request('redirect'));
	return generer_url_action('instituer_forum', "arg=$arg-off&hash=$hash&redirect=$r");
	include_spip('inc/invalideur');
	suivre_invalideur("id='id_forum/a$id_article'");
}

function spam_forum($id_forum, $r = '') {
	include_spip("inc/securiser_action");
	list($id_auteur, $pass) = caracteriser_auteur();
	$arg = "$id_forum";
	$hash = _action_auteur("instituer_forum-$arg-spam", $id_auteur, $pass, 'alea_ephemere');
	$r = rawurlencode(_request('redirect'));
	return generer_url_action('instituer_forum', "arg=$arg-spam&hash=$hash&redirect=$r");
	include_spip('inc/invalideur');
	suivre_invalideur("id='id_forum/a$id_article'");
}
