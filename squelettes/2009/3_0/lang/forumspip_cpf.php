<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/forumspip?lang_cible=cpf
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'avertissementtitre' => 'Véy byin ou la donn inn titr prési é klèr pou zot késtyon.Akoz i va rann pli fasil la navigasyon tout bann zot vizitèr dann bann forom.',

	// I
	'interetquestion' => 'Di anou koman ou pans késtyon la lé itil pou zot',
	'interetreponse' => 'Di anou koman ou pans répons-la lé itil pou zot',

	// N
	'nouvellequestion' => 'Poz inn nouvèl késtyon',
	'nouvellereponse' => 'Réponn késtyon-la',

	// Q
	'questions' => 'Bann késtyon',
	'quoideneuf' => 'Kosa la sanz bann dérnyé tan',

	// R
	'rechercher' => 'Pou rod',
	'rechercher_forums' => 'Rod andan bann forom',
	'rechercher_tout_site' => 'lo sit antyé',
	'reponses' => 'Bann répons',

	// T
	'thememessage' => 'Lo tèm forom-la i abord :',
	'traductions' => 'Bann tradiksyon pou lo téks-la :'
);
