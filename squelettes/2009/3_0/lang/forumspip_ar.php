<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/forumspip?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'هذا المفتاح لا يرتبط بأي رسالة في هذه اللغة.',
	'aucune_reponse' => 'بدون رد',
	'avertissement_code_forum' => 'لإدراج رموز برمجية او ابراز حلولك، يمكنك استخدام اختصارات الكتابة التالية:<ul><li>&lt;code&gt; ... سطر او اكثر من الرموز ... &lt;/code&gt;</li><li>&lt;cadre&gt; ... رموز برمجية تحتل سطوراً طويلة جداً ... &lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>ملاحظة</b> ان منتديات هذا الموقع نشطة جداً. فليشكر جميع الذين يتكفلون بإحياء مواقع التعاون هذه واثرائها.<p>الا انه كلما ازداد نشاط هذه المنتدبات كلما صعبت متابعتها وقراءتها. ولجعل هذه المنتديات اكثر متعة، نرجوك اتباع هذه التوصيات:<br /><img src=\'puce_rtl.gif\' border=\'0\' /> قبل اطلاق نقاش جديد، نرجوك التأكد من ان احداً لم يتطرق الى موضوعه من قبل في المنتدى؛<br /><img src=\'puce_rtl.gif\' border=\'0\' /> اعمل جهدك لطرح سؤالك في القسم المخصص له.',
	'avertissementtitre' => '<p>تأكد من انك ادخلت <strong>عنواناً واضحاً لسؤالك</strong> لتسهيل تصفح الزوار للمنتدى في ما بعد.</p> <p><strong>فالرسائل التي لا تحمل عنواناً واضحاً ستحذف.</strong></p>',

	// B
	'barre_cadre_html' => 'وضع في إطار وتلوين <cadre class=\'html4strict\'>علامات html</cadre>',
	'barre_cadre_php' => 'وضع في إطار وتلوين <cadre class=\'php\'>رموز php</cadre>',
	'barre_cadre_spip' => 'وضع في إطار وتلوين <cadre class=\'spip\'>رموز spip</cadre>',
	'barre_code' => 'إدراج رموز برمجية',
	'barre_inserer_code' => 'إدراج، وضع في إطار، تلوين رموز',
	'barre_quote' => 'اقتباس <quote>رسالة</quote>',

	// C
	'classer' => 'ترتيب',
	'clos' => 'خيط نقاش مقفل',

	// D
	'deplacer_dans' => 'نقل الى',
	'derniere_connexion' => 'آخر اتصال:',
	'derniers' => 'أحدث المشاركات',
	'download' => 'تحميل أحدث اصدار',

	// F
	'facultatif' => 'اختياري',
	'faq' => 'أسئلة وأجوبة',
	'faq_descriptif' => 'المواضيح التي تم حلها والاكثرإشارة من قبل الزوار',
	'forum_attention_explicite' => 'هذا العنوان ليس واضحاً، الرجاء توضيحه:',
	'forum_invalide_titre' => 'خيط نقاش معطل',
	'forum_modere_titre' => 'هذا الموضوع بانتظار التصديق',
	'forum_votre_email' => 'عنوان بريدك الالكتروني (اذا كنت ترغب في الحصول على الردود):',

	// G
	'galaxie' => 'في مجرة SPIP',

	// I
	'info_ajouter_document' => 'يمكنك إرفاق لقطة شاشة برسالتك',
	'info_connexion' => 'يتيح تحرير رسالة خلال ساعة واحدة',
	'info_ecrire_auteur' => 'يجب ان تكونون متصلين لإرسال رسالة خاصة:',
	'info_envoyer_message_prive' => 'يتيح إرسال رسائل خاصة للمشاركين المسجلين',
	'info_tag_forum' => 'يمكنك تعليم صفحة المنتدى هذه بواسطة مفاتيح تعتبرها مناسبة. وستتيح هذه المفاتيح سهولة اكبر للزوار المستقبليين في الاهتداء الى ما يهمهم:',
	'infos_stats_personnelles' => 'تيتح الاطلاع على معلومات الاتصال الشخصية',
	'interetquestion' => 'حدد مدى اهتمامك بهذا السؤال',
	'interetreponse' => 'حدد مدى اهتمامك بهذا الجواب',
	'inutile' => 'غير مفيدة',

	// L
	'liens_utiles' => 'روابط مفيدة',
	'login_login2' => 'المعرّف',

	// M
	'meme_sujet' => 'حول الموضوع نفسه',
	'merci' => 'شكراً',
	'messages' => 'مشاركة',
	'messages_auteur' => 'مشاركات هذا المؤلف:',
	'messages_connexion' => 'المشاركات منذ آخر اتصال:',

	// N
	'navigationrapide' => 'تصفح سريع:',
	'nb_sujets_forum' => 'المواضيع',
	'nb_sujets_resolus' => 'المواضيع التي تم حلها',
	'nouvellequestion' => 'طرح سؤال جديد',
	'nouvellereponse' => 'الاجابة على السؤال',

	// P
	'page_utile' => 'هل وجدت ان هذه الصفحة كانت: ',
	'par_date' => 'حسب التاريخ',
	'par_interet' => 'حسب الأهمية',
	'par_pertinence' => 'حسب الصلة',

	// Q
	'questions' => 'أسئلة',
	'quoideneuf' => 'احدث التعديلات',

	// R
	'rechercher' => 'بجث',
	'rechercher_forums' => 'البحص في المنتيات',
	'rechercher_tout_site' => 'كامل الموقع',
	'reponses' => 'أجوبة',
	'resolu' => 'تم الحل',
	'resolu_afficher' => 'عرض المشاركات المرتبطة بالمفتاح «تم الحل» فقط',
	'resolu_masquer' => 'عرض كل النتائج',
	'resolu_non' => 'غير محلول',

	// S
	'statut' => 'الوضعية:',
	'suggestion' => 'قبل المتابعة، هل اطلعت على الصفحات التالية؟ قد تحتوي هذه الصفحات على الإجابات التي تبحث عنها.',
	'suivi_thread' => 'ترخيص خيط النقاش هذا',
	'sujets_auteur' => 'مواضيع هذا المؤلف:',

	// T
	'thememessage' => 'موضوع هذا المنتدى:',
	'toutes_langues' => 'في كل اللغات',
	'traductions' => 'ترجمات هذا النص:',

	// U
	'utile' => 'مفيدة'
);
