<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/forumspip?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Esta palabra clave no está unida a ningún mensaje en esta lengua.',
	'aucune_reponse' => 'sin respuesta',
	'avertissement_code_forum' => 'Para insertar código o valorizar tus soluciones, puedes utilizar los atajos tipográficos  siguientes:<ul><li><code>... una o varias líneas de código...</code></li><li><cadre>... código que tiene líneas muy largas ...</cadre></li></ul>',
	'avertissementforum' => '<b>OjO</b> Los foros de este sitio son muy activos. Ante todo, se agradece a quienes animan y enriquecen estos espacios de ayuda mutua. <p>No obstante, cuanto más activos son los foros, más son dificiles de seguir y de consultar. Para que se vuelvan más agradables te agradecemos sigas las siguientes recomendaciones:   <br /><img src=\'puce.gif\' border=\'0\' /> antes de lanzar un nuevo tema de discusión, verifica que no fué abordado anteriormente;<br /><img src=\'puce.gif\' border=\'0\' />vigila que haces tu pregunta en la sección que corresponde.',
	'avertissementtitre' => '<p> Asegúrese de poner su pregunta <strong> en la sección adecuada</strong>e intente darle  <strong>un título  explícito</strong> a su pregunta, para luego facilitar la navegación de los visitantes de los foros. </p> <p><strong>Los mensajes cuyo título no sea explícito serán suprimidos.</strong></p>',

	// B
	'barre_cadre_html' => 'Encuadrar y colorear <cadre class=\'html4strict\'>el código  html</cadre>', # me imagino que las balizas   no hay que traducirlas ;)
	'barre_cadre_php' => 'Encuadrar y colorear <cadre class=\'php\'>el código php</cadre>',
	'barre_cadre_spip' => 'Encuadrar y colorear  <cadre class=\'spip\'>el código spip</cadre>',
	'barre_code' => 'Insertar código',
	'barre_inserer_code' => 'Insertar, encuadrar, colorear el código',
	'barre_quote' => 'Citar  <quote>un mensaje</quote>',

	// C
	'classer' => 'Clasificar',
	'clos' => 'Este hilo de discusión está cerrado',

	// D
	'deplacer_dans' => 'Desplazar en',
	'derniere_connexion' => 'Última conexión :',
	'derniers' => 'Últimos mensajes ',
	'download' => 'Descargar la última versión',

	// F
	'facultatif' => 'facultativo', # podría traducirse también por opcional
	'faq' => 'FAQ', # FAQ tanto en inglés como en francés quiere decir : Preguntas frecuentes. No obstante lo he dejado como FAQ, pues creo que es un término conocido de los internautas.
	'faq_descriptif' => 'Temas resueltos con  mejor nota atribuida por los visitantes',
	'forum_attention_explicite' => 'Este título no es lo bastante explícito, intente darle un título mas adecuado.',
	'forum_invalide_titre' => 'Este hilo de discusión ha sido invalidado.',
	'forum_votre_email' => 'Su dirección email (si usted desea recibir las respuestas) :',

	// G
	'galaxie' => 'En  la galaxia SPIP',

	// I
	'info_ajouter_document' => 'Usted puede adjuntar  une captura de pantalla a su mensaje ',
	'info_connexion' => 'Permite  editar su mensaje durante una hora',
	'info_ecrire_auteur' => 'Usted debe estar conectado-a para enviar un mensaje privado :',
	'info_envoyer_message_prive' => 'permite enviar mensajes privados a los contribuyentes inscritos',
	'info_tag_forum' => 'Puede etiquetar esta página de foro con palabras claves que le parezcan mas adecuadas; lo cual permitirá a los próximos visitantes ubicarse mejor.', # MODIF
	'infos_stats_personnelles' => 'permite consultar su información personal de conexión ',
	'interetquestion' => 'Indica el interés que tiene para ti esta pregunta ',
	'interetreponse' => 'Indica el interés que tiene para ti esta respuesta ',
	'inutile' => 'inútil',

	// L
	'liens_utiles' => 'Vínculos útiles',
	'login_login2' => 'Login', # se podría también traducir por 'usuario' pero he preferido dejarlo tal cual

	// M
	'meme_sujet' => 'Sobre el mismo tema',
	'merci' => 'gracias',
	'messages' => 'mensajes',
	'messages_auteur' => 'Mensajes de este autor :',
	'messages_connexion' => 'Mensajes desde la última conexión :',

	// N
	'navigationrapide' => 'Navegación rápida :',
	'nb_sujets_forum' => 'Temas',
	'nb_sujets_resolus' => 'Temas solucionados',
	'nouvellequestion' => 'Hacer una nueva pregunta',
	'nouvellereponse' => 'Responder a la pregunta',

	// P
	'page_utile' => 'Esta página fue para tí:',
	'par_date' => 'por fecha',
	'par_interet' => 'por interés',
	'par_pertinence' => 'por pertinencia',

	// Q
	'questions' => 'Preguntas',
	'quoideneuf' => 'Modificaciones recientes',

	// R
	'rechercher' => 'Buscar',
	'rechercher_forums' => 'Buscar en los foros',
	'rechercher_tout_site' => 'el sitio entero',
	'reponses' => 'Respuestas',
	'resolu' => 'Solucionado', # podía igualmente haber puesto 'resuelto', pero he preferido 'solucionado'
	'resolu_afficher' => 'Mostrar primero los mensajes vinculados a la palabra clave   « solucionado »',
	'resolu_masquer' => 'Mostrar todos los resultados',

	// S
	'statut' => 'Estatuto :',
	'suggestion' => 'Sugestión',
	'suivi_thread' => 'Sindicar este hilo de foro ',
	'sujets_auteur' => 'Temas de discusión de este autor :',

	// T
	'thememessage' => 'Tema de este foro:',
	'toutes_langues' => 'En todas las lenguas',
	'traductions' => 'Traducción de este texto:',

	// U
	'utile' => 'útil'
);
