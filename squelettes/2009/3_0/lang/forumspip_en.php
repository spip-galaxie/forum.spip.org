<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/forumspip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'This keyword is not attached to any message in this language.',
	'aucune_reponse' => 'No reply',
	'avertissement_code_forum' => 'To insert some code or ... ou mettre en valeur vos solutions, vous pouvez utiliser les raccourcis typographiques suivants :&lt;ul&gt;&lt;li&gt;&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;&lt;/li&gt;&lt;li&gt;&lt;cadre&gt;... code ayant des lignes très longues ...&lt;/cadre&gt;&lt;/li&gt;&lt;/ul&gt;',
	'avertissementforum' => '<b>P.S.</b> The forums of this site are vey active. Many thanks to those who animate and enrich these areas of mutual assistance.<p>However, the more the forums are active, the more dificult to follow and to consult they become. To turn these forums into a really exhilarating experience, we would be grateful if you follow these recommendations:<br /><img src=\'puce.gif\' border=\'0\' /> before starting a new discussion topic, please make sure that the subject has not been discussed here earlier;<br /><img src=\'puce.gif\' border=\'0\' /> make sure you ask your question in the section dedicated to it.',
	'avertissementtitre' => '<p>Make sure you give <strong>a clear title to your question</strong> in order to help other visitors navigate the forums.</p><p><strong>Messages without clear titles are deleted.</strong></p>',

	// B
	'barre_cadre_html' => 'Enclose and colour the <cadre class=\'html4strict\'>HTML code</cadre>',
	'barre_cadre_php' => 'Enclose and colour the <cadre class=\'php\'>PHP code</cadre>',
	'barre_cadre_spip' => 'Enclose and colour the <cadre class=\'spip\'>SPIP code</cadre>',
	'barre_code' => 'Insert <code>some code</code>',
	'barre_inserer_code' => 'Insert, enclose, and colour some code',
	'barre_quote' => 'Quote <quote>a message</quote>',

	// C
	'classer' => 'Sort',
	'clos' => 'This discussion thread is closed',

	// D
	'deplacer_dans' => 'Move to',
	'derniere_connexion' => 'Last logon:',
	'derniers' => 'Latest messages',
	'download' => 'Download the latest version',

	// F
	'facultatif' => 'optional',
	'faq' => 'FAQ',
	'faq_descriptif' => 'Topics solved best rated by the visitors',
	'forum_attention_explicite' => 'This title is not explicit enough, please detail it further:',
	'forum_invalide_titre' => 'This message thread has been invalidated',
	'forum_modere_titre' => 'This subject is waiting for validation',
	'forum_votre_email' => 'Your e-mail address (if you wish to receive the replies):',

	// G
	'galaxie' => 'In the SPIP universe',

	// I
	'info_ajouter_document' => 'You can attach a screen dump to your message',
	'info_connexion' => 'Allows you to edit your message for up to one hour',
	'info_ecrire_auteur' => 'You should be logged to send a private message:',
	'info_envoyer_message_prive' => 'allows to send a private message to the others registered contributors',
	'info_tag_forum' => 'You can tag this forum page with the keywords that you think are the most appropriate. This will help future visitors to find answers more quickly.',
	'infos_stats_personnelles' => 'allows access to your personal login informations',
	'interetquestion' => 'Please specify your interest in this question',
	'interetreponse' => 'Please specify your interest in this answer',
	'inutile' => 'useless',

	// L
	'liens_utiles' => 'Useful links',
	'login_login2' => 'Login',

	// M
	'meme_sujet' => 'On the same topic',
	'merci' => 'thank you',
	'messages' => 'messages',
	'messages_auteur' => 'Messages from this author:',
	'messages_connexion' => 'Messages since your last logon:',

	// N
	'navigationrapide' => 'Quick navigation:',
	'nb_sujets_forum' => 'Topics',
	'nb_sujets_resolus' => 'Topics solved',
	'nouvellequestion' => 'Ask a new question',
	'nouvellereponse' => 'Answering the question',

	// P
	'page_utile' => 'Did you find this page:',
	'par_date' => 'by date',
	'par_interet' => 'by rating',
	'par_pertinence' => 'by relevancy',

	// Q
	'questions' => 'Questions',
	'quoideneuf' => 'Recent changes',

	// R
	'rechercher' => 'Search',
	'rechercher_forums' => 'Search in forums',
	'rechercher_tout_site' => 'the whole site',
	'reponses' => 'Replies',
	'resolu' => 'Solved',
	'resolu_afficher' => 'Display only the messages linked to the keyword «solved»',
	'resolu_masquer' => 'Display all the results',
	'resolu_non' => 'Unresolved',

	// S
	'statut' => 'Status:',
	'suggestion' => 'Before continuing, have you consulted the following pages? Perhaps they might provide with the answer you seek.',
	'suivi_thread' => 'Syndicate this thread',
	'sujets_auteur' => 'Topics by this author:',

	// T
	'thememessage' => 'This forum’s theme:',
	'toutes_langues' => 'In all languages',
	'traductions' => 'Translations of this text:',

	// U
	'utile' => 'useful'
);
