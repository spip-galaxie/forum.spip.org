<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_galaxie_/forum.spip.org/squelettes/2009/3_0/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Ce mot-clé n’est attaché à aucun message dans cette langue.',
	'aucune_reponse' => 'Pas de réponse',
	'avertissement_code_forum' => 'Pour insérer du code ou mettre en valeur vos solutions, vous pouvez utiliser les raccourcis typographiques suivants :<ul><li>&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... code ayant des lignes très longues ...&lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>N.B.</b> Les forums de ce site sont très actifs. Que tous ceux qui animent et enrichissent ces espaces d’entraide soient ici remerciés.<p>Cependant, plus les forums sont actifs, et plus ils deviennent difficiles à suivre et à consulter. Pour rendre ces forums plus agréables, nous vous remercions de suivre ces recommandations :<br /><img src=\'puce.gif\' border=\'0\' /> avant de lancer un nouveau sujet de discussion, merci de vérifier que ce sujet n’a pas déjà été abordé ici ;<br /><img src=\'puce.gif\' border=\'0\' /> prenez soin de poser votre question dans la rubrique qui lui est consacrée.',
	'avertissementtitre' => '<p>Assurez-vous de poser votre question <strong>dans la rubrique appropriée</strong> et prenez soin de lui donner <strong>un titre explicite</strong> pour faciliter ensuite la navigation des autres visiteurs dans les forums.</p> <p><strong>Les messages dont le titre n’est pas explicite sont supprimés.</strong></p>',

	// B
	'barre_cadre_html' => 'Encadrer et colorer <cadre class=\'html4strict\'>du code html</cadre>',
	'barre_cadre_php' => 'Encadrer et colorer <cadre class=\'php\'>du code php</cadre>',
	'barre_cadre_spip' => 'Encadrer et colorer <cadre class=\'spip\'>du code spip</cadre>',
	'barre_code' => 'Insérer &lt;code&gt;du code&lt;/code&gt;',
	'barre_inserer_code' => 'Insérer, encadrer, colorer du code',
	'barre_quote' => 'Citer <quote>un message</quote>',

	// C
	'classer' => 'Classer',
	'clos' => 'Ce fil de discussion est clos',

	// D
	'deplacer_dans' => 'Déplacer dans',
	'derniere_connexion' => 'Dernière connexion :',
	'derniers' => 'Derniers messages',
	'download' => 'Télécharger la dernière version',

	// F
	'facultatif' => 'facultatif',
	'faq' => 'FAQ',
	'faq_descriptif' => 'Sujets résolus les mieux notés par les visiteurs',
	'forum_attention_explicite' => 'Ce titre n’est pas assez explicite, veuillez le préciser :',
	'forum_invalide_titre' => 'Ce fil de messages a été invalidé',
	'forum_modere_titre' => 'Ce sujet est en attente de validation',
	'forum_votre_email' => 'Votre adresse email (si vous souhaitez recevoir les réponses) :',

	// G
	'galaxie' => 'Dans la galaxie SPIP',

	// I
	'info_ajouter_document' => 'Vous pouvez joindre une capture d’écran à votre message',
	'info_connexion' => 'Permet d’éditer son message pendant une heure',
	'info_ecrire_auteur' => 'Vous devez être connecté-e pour envoyer un message privé :',
	'info_envoyer_message_prive' => 'permet d’envoyer des messages privés aux contributeurs enregistrés',
	'info_tag_forum' => 'Vous pouvez étiqueter cette page de forum avec les mots-clés qui vous semblent les plus appropriés ; ils permettront aux prochains visiteurs du site de mieux se repérer :',
	'infos_stats_personnelles' => 'permet de consulter ses informations de connexion personnelles',
	'interetquestion' => 'Indiquez l’intérêt que vous portez à cette question',
	'interetreponse' => 'Indiquez l’intérêt que vous portez à cette réponse',
	'inutile' => 'inutile',

	// L
	'liens_utiles' => 'Liens utiles',
	'login_login2' => 'Login',

	// M
	'meme_sujet' => 'Sur le même sujet',
	'merci' => 'merci',
	'messages' => 'messages',
	'messages_auteur' => 'Messages de cet auteur :',
	'messages_connexion' => 'Messages depuis la dernière connexion :',

	// N
	'navigationrapide' => 'Navigation rapide :',
	'nb_sujets_forum' => 'Sujets',
	'nb_sujets_resolus' => 'Sujets résolus',
	'nouvellequestion' => 'Poser une nouvelle question',
	'nouvellereponse' => 'Répondre à la question',

	// P
	'page_utile' => 'Cette page vous a-t-elle été :',
	'par_date' => 'par date',
	'par_interet' => 'par intérêt',
	'par_pertinence' => 'par pertinence',

	// Q
	'questions' => 'Questions',
	'quoideneuf' => 'Modifications récentes',

	// R
	'rechercher' => 'Rechercher',
	'rechercher_forums' => 'Rechercher dans les forums',
	'rechercher_tout_site' => 'tout le site',
	'reponses' => 'Réponse(s)',
	'resolu' => 'Résolu',
	'resolu_afficher' => 'Afficher seulement les résultats liés au mot-clé « résolu »',
	'resolu_masquer' => 'Afficher tous les résultats',
	'resolu_non' => 'Non résolu',

	// S
	'statut' => 'Statut :',
	'suggestion' => 'Avant de continuer, avez-vous consulté les pages suivantes ? Elles contiennent peut-être la réponse que vous cherchez.',
	'suivi_thread' => 'Syndiquer ce fil de forum',
	'sujets_auteur' => 'Sujets de cet auteur :',

	// T
	'thememessage' => 'Thème de ce forum :',
	'toutes_langues' => 'Dans toutes les langues',
	'traductions' => 'Traductions de ce texte :',

	// U
	'utile' => 'utile'
);
