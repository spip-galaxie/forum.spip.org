<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/forumspip?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Dit trefwoord is niet aan een bericht in deze taal gekoppeld.',
	'aucune_reponse' => 'Geen antwoord',
	'avertissement_code_forum' => 'Om code toe te voegen of je oplossing te verduidelijken, kun je de volgende typgrafische snelkoppelingen gebruiken:<ul><li>&lt;code&gt;... een of meer regels code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... code met zeer lange regels ...&lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>NB</b> De forums van deze site zijn zeer actief. Iedereen die daaraan bijdraagt wordt hiervoor bedankt.<p>Maar hoe intersiever een forum wordt gebruikt, hoe moeilijker het wordt een forum te volgen of te raadplegen. Daarom vragen we je de volgende aanbevelingen op te volgen:<br /><img src=\'puce.gif\' border=\'0\' /> controleer voor je een nieuwe discussie begint of het onderwerp niet al bestaat;<br /><img src=\'puce.gif\' border=\'0\' /> zorg ervoor dat je je vraag in de juiste rubriek stelt.',
	'avertissementtitre' => '<p>Zorg ervoor dat je je vraag <strong>in de juiste rubriek</strong> stelt en geef hem een <strong>expliciete titel</strong> zodat andere gebruikers deze duidelijk herkennen.</p> <p><strong>Berichten zonder expliciete titel worden verwijderd.</strong></p>',

	// B
	'barre_cadre_html' => 'Omkaderen en kleuren <cadre class=\'html4strict\'>van HTML-code</cadre>',
	'barre_cadre_php' => 'Omkaderen en kleuren <cadre class=\'php\'>van PHP-code</cadre>',
	'barre_cadre_spip' => 'Omkaderen en kleuren <cadre class=\'spip\'>van SPIP-code</cadre>',
	'barre_code' => 'Invoegen &lt;code&gt;van code&lt;/code&gt;',
	'barre_inserer_code' => 'Invoegen, omkaderen en kleuren van code',
	'barre_quote' => 'Citeren van <quote>een bericht</quote>',

	// C
	'classer' => 'Klasseren',
	'clos' => 'Deze discussiedraad is gesloten',

	// D
	'deplacer_dans' => 'Verplaatsen naar',
	'derniere_connexion' => 'Laatste verbinding:',
	'derniers' => 'Laatste berichten',
	'download' => 'De laatste versie downloaden',

	// F
	'facultatif' => 'facultatief',
	'faq' => 'FAQ',
	'faq_descriptif' => 'Best de bezoekers beoordeelde oplossingen',
	'forum_attention_explicite' => 'Deze titel is niet expliciet genoeg, verduidelijk ze:',
	'forum_invalide_titre' => 'Deze draad van berichten is ongeldig verklaard',
	'forum_modere_titre' => 'Dit onderwerp wacht op validatie',
	'forum_votre_email' => 'Je e-mailadres (wanneer je reacties wilt ontvangen):',

	// G
	'galaxie' => 'In het stelsel van SPIP',

	// I
	'info_ajouter_document' => 'Je kunt een schermafbeelding aan je bericht toevoegen',
	'info_connexion' => 'Het bericht kan één uur lang worden aangepast',
	'info_ecrire_auteur' => 'Om een privébericht te kunnen versturen, moet je zijn aangemeld:',
	'info_envoyer_message_prive' => 'je kunt privéberichten naar geregistreerde gebruikers sturen',
	'info_tag_forum' => 'Je kunt deze forumpagina voorzien van trefwoorden die je toepasselijk lijken. Hierdoor kunnen toekomstige bezoekers zich beter oriënteren:',
	'infos_stats_personnelles' => 'je kunt je persoonlijke verbindingsgegevens bekijken',
	'interetquestion' => 'Geef het belang aan dat je aan deze vraag hebt',
	'interetreponse' => 'Geef het belang aan dat je aan dit antwoord hebt',
	'inutile' => 'nutteloos',

	// L
	'liens_utiles' => 'Nuttige koppelingen',
	'login_login2' => 'Login',

	// M
	'meme_sujet' => 'Met hetzelfde onderwerp',
	'merci' => 'bedankt',
	'messages' => 'berichten',
	'messages_auteur' => 'Berichten van deze auteur:',
	'messages_connexion' => 'Berichten sindsde laatste verbinding:',

	// N
	'navigationrapide' => 'Snelle navigatie:',
	'nb_sujets_forum' => 'Onderwerpen',
	'nb_sujets_resolus' => 'Opgeloste onderwerpen',
	'nouvellequestion' => 'Een nieuwe vraag stellen',
	'nouvellereponse' => 'De vraag beantwoorden',

	// P
	'page_utile' => 'Deze pagina was voor jou:',
	'par_date' => 'per datum',
	'par_interet' => 'per belang',
	'par_pertinence' => 'per relevantie',

	// Q
	'questions' => 'Vragen',
	'quoideneuf' => 'Recente aanpassingen',

	// R
	'rechercher' => 'Zoeken',
	'rechercher_forums' => 'In de forums zoeken',
	'rechercher_tout_site' => 'de hele site',
	'reponses' => 'Antwoord(en)',
	'resolu' => 'Opgelost',
	'resolu_afficher' => 'Toon uitsluitend resultaten met het trefwoord «opgelost»',
	'resolu_masquer' => 'Toon alle resultaten',
	'resolu_non' => 'Niet opgelost',

	// S
	'statut' => 'Status:',
	'suggestion' => 'Heb je, voor je verder gaat, deze pagina’s bekeken? Ze bevatten mogelijk een antwoord op jouw vraag.',
	'suivi_thread' => 'Deze forumdraad syndiceren',
	'sujets_auteur' => 'Onderwerpen van deze auteur:',

	// T
	'thememessage' => 'Thema van dit forum:',
	'toutes_langues' => 'In alle talen',
	'traductions' => 'Vertalingen van deze tekst:',

	// U
	'utile' => 'nuttig'
);
