<?php


define('_ID_WEBMESTRES', '3151:2225');

//ajoutons le repertoire plugin dans squelettes 
define('_DIR_PLUGINS_SUPPL',_DIR_RACINE."squelettes/plugins/");

// Pour tester : envoi des mails sur les threads
// cf. inc/notifications.php
define('_SUIVI_FORUM_THREAD', true);

include _DIR_RACINE . 'squelettes/urls/trad.php';

// URLs de la forme /fr_article1.html (inspire de www.spip.net)
$type_urls = 'trad';

// Ajouter la barre typo speciale (<code> et <cadre>)
$barre_typo = 'forumspiporg';

// augmenter le cache
$quota_cache = 50;

// longueur max message
define('_FORUM_LONGUEUR_MINI', 3);
