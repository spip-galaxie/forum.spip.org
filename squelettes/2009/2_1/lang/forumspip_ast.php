<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forumspip?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Esta pallabra clave nun s\'amestó a dengún mensaxe nesta llingua',
	'aucune_reponse' => 'Nun hai rempuestes',
	'avertissement_code_forum' => 'Pa inxertar du códigu o amosar soluciones, pueden utilizase los siguientes atayos tipográficos suivants:<ul><li>&lt;code&gt;... una o más llinies de códigu ...&lt;/code&gt;</li><li>&lt;cadre&gt;... códigu que tenga llinies mui llargues ...&lt;/cadre&gt;</li></ul>', # MODIF
	'avertissementforum' => '<b>Nota</b> Los foros d\'esti sitiu son mui activos. Damos-yos les gracies a toles persones que animen y arriquecen esti espaciu d\'aida mutua.<p>Sicasí, cuanto más activos tean los foros, más abegoso ye siguilos y consultalos. Pa facer estos foros más agradables, agradecemos siguir estes recomendaciones:<br><img src=\'puce.gif\' border=\'0\'> enantes de llanzar un nuevu filu de discusión, comprobar que l\'asuntu nun tea yá tratáu equí;<br><img src=\'puce.gif\' border=\'0\'> tener procuru pa poner la entruga na estaya que tenga dedicada.',
	'avertissementtitre' => '<p>Asegúrese de poner la entruga <strong>na estaya afayadiza</strong> y tenga procuru pa da-y <strong>un títulu claru</strong> pa lluéu facilitar la navegación de otres visites polos foros.</p> <p><strong>Los mensaxes que nun tengan un títulu claru desaniciaránse.</strong></p>',

	// B
	'barre_cadre_html' => 'Encuadrar y da-y color &lt;cadre class=\'html4strict\'&gt;al códigu html&lt;/cadre&gt;',
	'barre_cadre_php' => 'Encuadrar y da-y color &lt;cadre class=\'php\'&gt;al códigu php&lt;/cadre&gt;',
	'barre_cadre_spip' => 'Encuadrar y da-y color &lt;cadre class=\'spip\'&gt;al códigu spip&lt;/cadre&gt;',
	'barre_code' => 'Amestar &lt;code&gt;códigu&lt;/code&gt;',
	'barre_inserer_code' => 'Amestar, encuadrar, y da-y color al códigu',
	'barre_quote' => 'Citar <quote>un mensaxe</quote>',

	// C
	'classer' => 'Clasificar',
	'clos' => 'Esti filu d\'alderique ta zarráu',

	// D
	'deplacer_dans' => 'Mover en',
	'derniere_connexion' => 'Dernière connexion :', # NEW
	'derniers' => 'Caberos mensaxes',
	'download' => 'Descargar la versión cabera',

	// F
	'facultatif' => 'facultatif', # NEW
	'faq' => 'FAQ', # NEW
	'faq_descriptif' => 'Sujets résolus les mieux notés par les visiteurs', # NEW
	'forum_attention_explicite' => 'Esti títulu nun ye mui esplícitu, tendría de precisalu:', # MODIF
	'forum_invalide_titre' => 'Esti filu de mensaxes ta invalidáu',
	'forum_votre_email' => 'Les sos señes de corréu (si quier recibir les rempuestes):', # MODIF

	// G
	'galaxie' => 'Na galaxa SPIP',

	// I
	'info_ajouter_document' => 'Puede amestase-y una captura de pantalla al mensaxe',
	'info_connexion' => 'Permite editar el mensaxe demientres una hora',
	'info_ecrire_auteur' => 'Vous devez être connecté-e pour envoyer un message privé :', # NEW
	'info_envoyer_message_prive' => 'permet d\'envoyer des messages privés aux contributeurs enregistrés', # NEW
	'info_tag_forum' => 'Puedes etiquetar esta páxina del foru coles pallabres clave que te paezan más afayadices; estes permitirán que los demás visitantes del sitiu puedan alcontrala meyor:', # MODIF
	'infos_stats_personnelles' => 'permet de consulter ses informations de connexion personnelles', # NEW
	'interetquestion' => 'Marque l\'interés que-y da a esta cuestión',
	'interetreponse' => 'Marque l\'interés que-y merez esta rempuesta',
	'inutile' => 'inutil',

	// L
	'liens_utiles' => 'Enllaces afayadizos',
	'login_login2' => 'Conexón',

	// M
	'meme_sujet' => 'Sobro\'l mesmu asuntu',
	'merci' => 'gracies',
	'messages' => 'mensaxes',
	'messages_auteur' => 'Messages de cet auteur :', # NEW
	'messages_connexion' => 'Messages depuis la dernière connexion :', # NEW

	// N
	'navigationrapide' => 'Ñavegación rápida:', # MODIF
	'nb_sujets_forum' => 'Sujets', # NEW
	'nb_sujets_resolus' => 'Sujets résolus', # NEW
	'nouvellequestion' => 'Facer una entruga nueva',
	'nouvellereponse' => 'Responder la entruga',

	// P
	'page_utile' => 'Esta páxina fue:',
	'par_date' => 'por data',
	'par_interet' => 'pol interés',
	'par_pertinence' => 'pola rellación',

	// Q
	'questions' => 'Entrugues',
	'quoideneuf' => 'Cambeos recientes',

	// R
	'rechercher' => 'Guetar',
	'rechercher_forums' => 'Guetar nos foros',
	'rechercher_tout_site' => 'tou el sitiu',
	'reponses' => 'Rempuesta(es)',
	'resolu' => 'Iguao',
	'resolu_afficher' => 'Amosar primero los mensaxes lligaos cola pallabra clave «iguao»', # MODIF
	'resolu_masquer' => 'Mazcarar los mensaxes lligaos cola pallabre clave «iguao»', # MODIF

	// S
	'statut' => 'Statut :', # NEW
	'suggestion' => 'Enantes de siguir, ¿tien consultaes les páxines darréu? Seique contengan la rempuesta a lo que va entrugar.',
	'suivi_thread' => 'Sindicar esti filu del foru',
	'sujets_auteur' => 'Sujets de cet auteur :', # NEW

	// T
	'thememessage' => 'Tema d\'esti foru:',
	'toutes_langues' => 'En toles llingües',
	'traductions' => 'Tornes d\'esti testu:',

	// U
	'utile' => 'útil'
);

?>
