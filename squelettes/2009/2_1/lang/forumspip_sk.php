<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forumspip?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Toto kľúčové slovo nie je pripojené k žiadnemu príspevku v tomto jazyku.',
	'aucune_reponse' => 'Bez reakcie',
	'avertissement_code_forum' => 'Ak chcete vložiť nejaký kód alebo zvýrazniť svoje riešienie, môžete použiť tieto klávesové skratky:<ul><li>&lt;kód&gt;... jeden alebo viac riadkov kódu ...&lt;/code&gt;</li><li>&lt;cadre&gt;... kód s veľmi dlhými riadkami ...&lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>P.S.</b> The forums of this site are vey active. Many thanks to those who animate and enrich these areas of mutual assistance.<p>However, the more the forums are active, the more dificult to follow and to consult they become. To turn these forums into a really exhilarating experience, we would be grateful if you follow these recommendations:<br><img src=\'puce.gif\' border=\'0\'> before starting a new discussion topic, please make sure that the subject has not been discussed here earlier;<br><img src=\'puce.gif\' border=\'0\'> make sure you ask your question in the section dedicated to it.',
	'avertissementtitre' => '<p>Make sure you give <strong>a clear title to your question</strong> in order to help other visitors navigate the forums.</p><p><strong>Messages without clear titles are deleted.</strong></p>',

	// B
	'barre_cadre_html' => 'Ohraničiť a vyfarbiť <cadre class=\'html4strict\'>HTML kó</cadre>',
	'barre_cadre_php' => 'Ohraničiť a vyfarbiť <cadre class=\'php\'>PHP kód</cadre>',
	'barre_cadre_spip' => 'Ohraničiť a vyfarbiť <cadre class=\'spip\'>kód SPIPU</cadre>',
	'barre_code' => 'Vložiť <code>nejaký kód</code>',
	'barre_inserer_code' => 'Vložiť, ohraničiť a vyfarbiť nejaký kód',
	'barre_quote' => 'Citovať <quote>príspevok</quote>',

	// C
	'classer' => 'Ohodnotiť',
	'clos' => 'Diskusia k tejto téme je uzavretá',

	// D
	'deplacer_dans' => 'Presunúť do',
	'derniere_connexion' => 'Posledné prihlásenie:',
	'derniers' => 'Najnovšie príspevky',
	'download' => 'Stiahnuť najnovšiu verziu',

	// F
	'facultatif' => 'nepovinné',
	'faq' => 'Časté otázky',
	'faq_descriptif' => 'Vyriešené témy, ktoré ohodnotili návštevníci',
	'forum_attention_explicite' => 'Tento názov nie je dosť jasný, prosím, upresnite ho:',
	'forum_invalide_titre' => 'Toto vlákno diskusie bolo odstránené',
	'forum_votre_email' => 'Vaša e-mailová adresa (ak chcete dostávať odpovede):',

	// G
	'galaxie' => 'V univerze SPIPU',

	// I
	'info_ajouter_document' => 'K správe môžete pripojiť výpis z obrazovky',
	'info_connexion' => 'Umožňuje vám upraviť príspevok do hodiny',
	'info_ecrire_auteur' => 'Ak chcete poslať súkromnú správu, musíte sa prihlásiť:',
	'info_envoyer_message_prive' => 'umožňuje vám posielať súkromné správy prihláseným prispievateľom',
	'info_tag_forum' => 'Túto stránku môžete označiť  ako diskusné fórum s kľúčovými slovami, o ktorých si myslíte, že sú najvýstižnejšie; budúcim návštevníkom to pomôže rýchlejšie nájsť odpovede:',
	'infos_stats_personnelles' => 'umožňuje zobraziť ich súkromné prihlasovacie údaje',
	'interetquestion' => 'Prosím, uveďte, ako vás zaujíma táto otázka',
	'interetreponse' => 'Prosím, uveďte, ako vás zaujíma táto odpoveď',
	'inutile' => 'neužitočná',

	// L
	'liens_utiles' => 'Užitočné odkazy',
	'login_login2' => 'Prihlasovacie meno',

	// M
	'meme_sujet' => 'Na rovnakú tému',
	'merci' => 'ďakujeme',
	'messages' => 'príspevky',
	'messages_auteur' => 'Správy tohto autora:',
	'messages_connexion' => 'Správy od posledného prihlásenia:',

	// N
	'navigationrapide' => 'Rýchla navigácia:',
	'nb_sujets_forum' => 'Témy',
	'nb_sujets_resolus' => 'Vyriešené témy',
	'nouvellequestion' => 'Položiť novú otázku',
	'nouvellereponse' => 'Odpovedať na otázku',

	// P
	'page_utile' => 'Našli ste túto stránku:',
	'par_date' => 'podľa dátumu',
	'par_interet' => 'podľa témy',
	'par_pertinence' => 'podľa relevantnosti',

	// Q
	'questions' => 'Otázky',
	'quoideneuf' => 'Najnovšie zmeny',

	// R
	'rechercher' => 'Vyhľadať',
	'rechercher_forums' => 'Vyhľadať v diskusných fórach',
	'rechercher_tout_site' => 'na celej stránke',
	'reponses' => 'Odpovede',
	'resolu' => 'Vyriešené',
	'resolu_afficher' => 'Zobraziť iba výsledky s kľúčovým slovom "vyriešené"',
	'resolu_masquer' => 'Zobraziť všetky výsledky',

	// S
	'statut' => 'Stav:',
	'suggestion' => 'Predtým, ako budete pokračovať, pozreli ste sa na tieto stránky? Možno vám poskytnú odpoveď, ktorú hľadáte.',
	'suivi_thread' => 'Syndikovať túto niť',
	'sujets_auteur' => 'Témy tohto autora:',

	// T
	'thememessage' => 'Téma tohto diskusného fóra:',
	'toutes_langues' => 'Vo všetkých jazykoch',
	'traductions' => 'Preklady tohto textu:',

	// U
	'utile' => 'užitočná'
);

?>
