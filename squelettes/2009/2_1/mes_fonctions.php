<?php

// appliquer supprimer_numero sur tous les titres
$GLOBALS['table_des_traitements']['TITRE'][]= 'typo(supprimer_numero(%s))';

// Toute personne censee se detournerait de la rfc 822... et pourtant
function date_rfc822($date_heure) {
	list($annee, $mois, $jour) = recup_date($date_heure);
	list($heures, $minutes, $secondes) = recup_heure($date_heure);
	$time = mktime($heures, $minutes, $secondes, $mois, $jour, $annee);
	$timezone = sprintf('%+03d',intval(date('Z')/3600)).'00';
	return date("D, d M Y H:i:s", $time)." $timezone";
}

// corriger les URLs gmane (on syndique 'blog....' mais on ne veut pas lier la-dessus)
function gmane($url) {
	return $url;
	return str_replace('http://comments.gmane.', 'http://thread.gmane.', $url);
}

// pour les forums
function raccourcir_nom($nom) {
	if (strpos($nom, "@")) {
	 $nom = substr($nom, 0, strpos($nom, "@"));	
}
return $nom;
}

// pour afficher proprement le nom des langues
function afficher_nom_langue ($lang) {
	if (ereg("^oc(_|$)", $lang))
 return "occitan";
	else
 return traduire_nom_langue($lang);
}

// pour rendre les dates insecables dans les pages forum
function insecable ($texte) {
	return ereg_replace("( |&nbsp;)+", "&nbsp;", $texte);
}

// supprimer les '> ' en début de titre de forum
function spip_preg_replace($a,$b,$c) {
	return preg_replace($b,$c,$a);
}


function police_des_bavards($score)
{
	return round((1+(log10($score)))/1.9,2);
}

// supprimer les mots-cles depuis l'espace public
function generer_moderer_mot($id_mot,$id_objet,$table='forum',$table_id='',$r='') {
   if (!$table_id) $table_id='id_'.substr($table,0,-1);
   include_spip("inc/securiser_action");
   list($id_auteur, $pass) =  caracteriser_auteur();
   $arg="$id_objet,$id_mot,forum,$table_id,objet";
   $hash = _action_auteur("editer_mots-$arg", $id_auteur, $pass, 'alea_ephemere');
   $r = rawurlencode(_request('redirect'));
   return generer_url_action('editer_mots', "arg=$arg&hash=$hash&redirect=$r");
   include_spip('inc/invalideur');
   suivre_invalideur("id='id_forum/a$id_article'");
}

// moderer les messages depuis l'espace public
// ne supprime pas le message ni le fil mais les passe en 'off'
// on peut toujours les revalider dans l'espace prive
function invalider_forum($id_forum,$r='') {
   include_spip("inc/securiser_action");
   list($id_auteur, $pass) =  caracteriser_auteur();
   $arg="$id_forum";
   $hash = _action_auteur("instituer_forum-$arg-off", $id_auteur, $pass, 'alea_ephemere');
   $r = rawurlencode(_request('redirect'));
   return generer_url_action('instituer_forum', "arg=$arg-off&hash=$hash&redirect=$r");
   include_spip('inc/invalideur');
   suivre_invalideur("id='id_forum/a$id_article'");
}

function spam_forum($id_forum,$r='') {
   include_spip("inc/securiser_action");
   list($id_auteur, $pass) =  caracteriser_auteur();
   $arg="$id_forum";
   $hash = _action_auteur("instituer_forum-$arg-spam", $id_auteur, $pass, 'alea_ephemere');
   $r = rawurlencode(_request('redirect'));
   return generer_url_action('instituer_forum', "arg=$arg-spam&hash=$hash&redirect=$r");
   include_spip('inc/invalideur');
   suivre_invalideur("id='id_forum/a$id_article'");
}

?>
